#include <iostream>

class Animal
{
public:
    Animal() {}
    virtual void Voice()
    {
        std::cout << "Om\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Mew\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo\n";
    }
};

class Goat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Bee\n";
    }
};

int main()
{
    Animal* arr[] = { new Cow, new Cat, new Animal, new Goat, new Cat };
    for (auto* elem : arr)
    {
        elem->Voice();
    }
}
